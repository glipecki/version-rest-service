package net.lipecki.blog.warwithversion;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Spring context configration with Web MVC support.
 * 
 * @author Grzegorz Lipecki
 */
@Configuration
@EnableWebMvc
public class RestConfiguration {

	/**
	 * Version REST controller bean.
	 * 
	 * @return
	 */
	@Bean
	public VersionController getVersionController() {
		return new VersionController(servletContext);
	}

	/**
	 * We need servlet context to get correct serlvet classloader to get manifest from war.
	 */
	@Autowired
	ServletContext servletContext;

}
