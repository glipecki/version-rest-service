package net.lipecki.blog.warwithversion;

import java.io.IOException;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import javax.servlet.ServletContext;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Application version REST service.
 * 
 * @author Grzegorz Lipecki
 */
@RequestMapping(value = "/version")
public class VersionController {

	public VersionController(final ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	/**
	 * Get deployed application version and build number.
	 * 
	 * @return human readable application version with build number
	 * @throws IOException
	 *             possible exception while loading manifest file
	 */
	@RequestMapping
	@ResponseBody
	public String version() throws IOException {
		final Attributes manifestAttributes = getManifestAttributes();

		final String implementationVersion = manifestAttributes.getValue("Implementation-Version");
		final String buildVersion = manifestAttributes.getValue("Implementation-Build");

		return "Version: " + implementationVersion + " Build: " + getBuildVersion(buildVersion);
	}

	/**
	 * Get Build-Version based on raw manifest attribute value.
	 * <p>
	 * For builded war maven will create war manifest with Implementation-Version and Implementation-Build. In IDE
	 * Implementation-Build will be empty.
	 * </p>
	 * 
	 * @param buildVersion
	 *            raw Build-Version attribute value from WAR manifest
	 * @return human readable build number
	 */
	private String getBuildVersion(final String buildVersion) {
		if (!buildVersion.trim().isEmpty()) {
			return buildVersion;
		} else {
			return "UNKNOWN build (running in wtp?)";
		}
	}

	/**
	 * Get WAR manifest main attributes.
	 * 
	 * @return manifest main attributes
	 * @throws IOException
	 *             possible exception while loading manifest file
	 */
	private Attributes getManifestAttributes() throws IOException {
		final Manifest manifest = new Manifest(servletContext.getResourceAsStream("/META-INF/MANIFEST.MF"));
		return manifest.getMainAttributes();
	}

	private final ServletContext servletContext;

}
